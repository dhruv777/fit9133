# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Sep 19, 2019
# Last Modified Date: Oct 17, 2019
# Task 3 - Calculating Inverse Document Frequency (IDF)
# Implement IDF calculations to categorize how important a term is within a corpus of documents

import pandas as pd
import numpy as np

class IDFAnalyser:
    """ 
    Class to implement IDF calculations. 
    
    Attributes
    ----------
    data : Pandas Dataframe
        Frequencies of the word for each cleaned text
    """
    def __init__(self):
        """
        Constructor to initialize word_counts to an empty DataFrame
        """
        self.data = pd.DataFrame()
        
    def load_frequency(self, book_frequency, book_title):
        """
        Load frequency of a cleaned text into the dataframe with the corresponding title

        Parameters
        ----------
        book_frequency : dict
            Word frequencies for a cleaned text
        book_title : str
            Title for the cleaned text
        """

        # Add the frequencies to the dataframe with the book title as the index for the row
        self.data = self.data.append(pd.Series(book_frequency, name = book_title))
        
    def get_IDF(self, term):
        """
        Obtain Inverse Document Frequency(IDF) for term provided and the documents loaded in the dataframe

        Parameters
        ----------
        term : str
            Term for which IDF has to be returned
            
        Returns
        -------
        IDF : float
            Inverse Document Frequency for the term passed as parameter
        """
        N = self.data[term].count()         # Count of number of documents with the term
        D = self.data.shape[0]              # Number of documents in the corpus
        IDF = 1 + np.log(D / (1 + N))       # Calculate IDF
        return IDF
