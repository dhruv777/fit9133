# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Sep 19, 2019
# Last Modified Date: Oct 17, 2019
# Task 4 - Presenting Your Results
# Use Term Frequency-Inverse Document Frequency (TF-IDF) to determine the most suitable document for a given term

def choice(term, document):
    """
    Get the name of the document having highest TF-IDF value for given term

    Parameters
    ----------
    term : str
        Term for which most suitable document is to be returned
    document : IDFAnalyser object
        idf object with books loaded in the dataframe
    """
    TF = document.data[term]            # Series with term frequencies for the term for all loaded documents
    IDF = document.get_IDF(term)        # Inverse document frequency for the term
    TF_IDF = TF * IDF                   # Calculate TF-IDF
    return TF_IDF.idxmax()
