# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Sep 19, 2019
# Last Modified Date: Oct 17, 2019
# Task 1 - Setting up the Preprocessor
# Perform basic preprocessing on each input text

class Preprocessor:
    """ 
    Class to perform basic preprocessing on input text
    
    Attributes
    ----------
    book_content : str
        Book text as a string
    """
    def __init__(self):
        """
        Constructor to initialize book_content to an empty string
        """
        self.book_content = str()

    def __str__(self):
        """
        Return book_content as a string
        """
        return self.book_content

    def clean(self):
        """
        Remove undesirable characters from book_content
        """
        if len(self.book_content) == 0:
            return 1                                # Return 1 if no text has been read

        clean_content = ""

        self.book_content = self.book_content.lower()   # Convert to lowercase

        for ch in self.book_content:
            if ord(ch) in range(ord('0'), ord('9') + 1) or ord(ch) in range(ord('a'), ord('z') + 1) or ch.isspace():
                clean_content += ch                 # Verify each character is alphabet, number or whitespace

            elif ch in '-_':
                clean_content += " "                # Replace hyphen and underscore with single whitespace

        self.book_content = clean_content
        return None

    def read_text(self, text_name):
        """
        Read contents of a file into the instance variable of the class

        Parameters
        ----------
        text_name : str
            Name of the file in the current directory
        """
        with open(text_name,'r', encoding = 'utf-8-sig') as f:
            self.book_content = f.read()

