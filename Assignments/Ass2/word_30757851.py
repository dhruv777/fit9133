# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Sep 19, 2019
# Last Modified Date: Oct 17, 2019
# Task 2 - Word Analyser
# Analyse the number of occurences of each word from a given cleaned text

class WordAnalyser:
    """ 
    Class to perform basic preprocessing on input text
    
    Attributes
    ----------
    word_counts : dict
        Words and number of occurences for the word
    """
    def __init__(self):
        """
        Constructor to initialize word_counts to an empty Dictionary
        """
        self.word_counts = dict()

    def __str__(self):
        """
        Return number of occurences of each word in a readable format
        """
        some = ""
        for key, value in self.word_counts.items():
            some += key + ":" + str(value) + "\n"
        return some

    def analyse_words(self, book_text):
        """
        Perform a count on a given book text at the word level and update in word_counts

        Parameters
        ----------
        book_text : str
            Cleaned book text
        
        """
        self.word_counts = dict()

        for word in book_text.split():
            if word in self.word_counts.keys():
                self.word_counts[word] += 1                             # Increment word count if word present
            else:
                self.word_counts[word] = 1                              # For new word assign 1

    def get_word_frequency(self):
        """
        Calculate and return frequency of the words.

        Returns
        ----------
        word_freq : dict
            Word and its frequency    
        
        """
        word_freq = {}
        
        total_word_count = sum(self.word_counts.values())              # Total number of words in the text

        for word in self.word_counts.keys():
            word_freq[word] = self.word_counts[word] / total_word_count
        
        return word_freq
