# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Aug 17, 2019
# Last Modified Date: Sep 8, 2019
# Simulating a Town - Task 3
# Read in town starting data, calculate population remaining after a single year
# cycle without considering changes to food and then write to a file

def read_town_file():
	"""
	Read town starting characteristics data from town_start.txt and
	return a list containing the starting food, population and year.
	"""
	with open("town_start.txt", "r") as rf:
		town_data = [line.strip() for line in rf.readlines()]
	return town_data

def write_town_file():
	"""
	Write the town ending state data in town_end.txt
	"""
	with open("town_end.txt", "w+") as wf:
		wf.write("{}\n{}\n{}".format(int(food), int(popn), year))

def calc_town_data(food, popn, year):
	"""
	Calculate changes in the town data after a single year's
	cycle and returns the ending state data. Doesn't consider
	changes to food.

	Args:
		food (int): Starting food
		popn (int): Starting population
		year (int): Starting year
	Returns:
		food (int): Remaining food
		popn (int): Remaining population
		year (int): Final year
	"""
	if popn == 0:			# Stop simulation if town's population at start of year is zero
		return food, popn, year

	if popn > food: 		# If food isn't enough, people leave town
		popn = food

	popn += 0.3 * popn 		# Immigration and natural births
	popn = int(popn)

	if year % 18 == 0:		# Football draft every 18 years
		popn -= 0.4 * popn
		popn = int(popn)

	year += 1				# Increment a year

	return food, popn, year

# ------ Main Program -------
town_data = read_town_file()
food, popn, year = map(int, town_data)	# Map and store the list data into different variables
food, popn, year = calc_town_data(food, popn, year)
write_town_file()
