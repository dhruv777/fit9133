# Author: Dhruv Gupta
# Student Id: 30757851
# Start Date: Aug 17, 2019
# Last Modified Date: Sep 8, 2019
# Simulating a Town - Task 1
# Read in town starting data, store in variables and then write to a file

def read_town_file():
	"""
	Read town starting characteristics data from town_start.txt and
	return a list containing the starting food, population and year.
	"""
	with open("town_start.txt", "r") as rf:
		town_data = [line.strip() for line in rf.readlines()]
	return town_data

def write_town_file():
	"""
	Write the town ending state data in town_end.txt
	"""
	with open("town_end.txt", "w+") as wf:
		wf.write("{}\n{}\n{}".format(int(food), int(popn), year))

# ------ Main Program -------
town_data = read_town_file()
food, popn, year = map(int, town_data)	# Map and store the list data into different variables
write_town_file()
